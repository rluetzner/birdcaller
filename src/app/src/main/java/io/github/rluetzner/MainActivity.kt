package io.codeberg.rluetzner

import android.Manifest
import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.os.Looper
import android.widget.Button
import android.widget.TextView
import android.widget.ToggleButton
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import io.paperdb.Paper
import kotlinx.coroutines.*
import java.time.LocalDateTime

class MainActivity : AppCompatActivity(), LocationListener {

    private lateinit var lm: LocationManager
    private var provider: String? = null

    // Variable for storing instance of our service class
    private var mService: AudioService? = null
    private var mIsBound: Boolean? = null

    private lateinit var config: Config

    // This job will refresh the suntimes continuously.
    private var job: Job? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Paper.init(applicationContext)
        setContentView(R.layout.activity_main)
        bindService()
        lm = applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        provider = LocationManager.GPS_PROVIDER

        job = GlobalScope.launch {
            while (mService == null) {
                delay(1000)
            }
            initDisplayedText()
            while (true) {
                updateSuntimes()
                delay(1000)
            }
        }

        val btnFileBrowser = findViewById<Button>(R.id.bt_filebrowser)
        btnFileBrowser.setOnClickListener {
            openFileChooser()
        }

        val sw = findViewById<ToggleButton>(R.id.toggleButton)
        sw.setOnCheckedChangeListener { _, isChecked ->
            toggleAudioPlayback(isChecked)
        }

        val btnCoordinates = findViewById<Button>(R.id.bt_coordinates)
        btnCoordinates.setOnClickListener {
            updateLocation()
        }
    }

    private fun openFileChooser() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).also {
            it.addCategory(Intent.CATEGORY_OPENABLE)
            it.type = "audio/*"
            it.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION)
            it.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        }
        resultLauncher.launch(intent)
    }

    private fun toggleAudioPlayback(isChecked: Boolean) {
        if (isChecked) {
            mService?.contentUri = Uri.parse(config.audioFile)
        } else {
            mService?.contentUri = null
        }
    }

    private suspend fun initDisplayedText() {
        withContext(Dispatchers.Main) {
            val cfg = Paper.book().read<Config>("config")
            if (cfg == null) {
                config = Config()
            } else {
                config = cfg
            }
            val toggleButton = findViewById<ToggleButton>(R.id.toggleButton)
            val label = findViewById<TextView>(R.id.tv_selectedFile)
            if (config.audioFile != null) {
                var uri = Uri.parse(config.audioFile)
                label.text = uri.path
                if (mService?.contentUri != null) {
                    toggleButton.isChecked = true
                }
                toggleButton.isEnabled = true
            } else {
                label.text = Constants.NO_AUDIO_FILE
                toggleButton.isChecked = false
                toggleButton.isEnabled = false
            }
            setLocation()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private suspend fun updateSuntimes() {
        val date = LocalDateTime.now()
        val midnight = LocalDateTime.of(date.year, date.month, date.dayOfMonth, 0, 0)
        val sunTime = mService?.getSuntime(midnight)
        withContext(Dispatchers.Main) {
            val sunup = findViewById<TextView>(R.id.tv_sunup)
            val sundown = findViewById<TextView>(R.id.tv_sundown)
            if (sunTime != null) {
                sunup.text = "Sunup is at " + sunTime?.rise
                sundown.text = "Sundown is at " + sunTime?.set
            } else {
                sunup.text = Constants.NO_LOCATION
                sundown.text = ""
            }
        }
    }

    // Callback for the FileChooser result
    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data = result.data
                val selectedFile = data?.data //The uri with the location of the file
                if (selectedFile != null) {
                    val label = findViewById<TextView>(R.id.tv_selectedFile)
                    config.audioFile = selectedFile.toString()
                    saveConfigAsync()
                    label.text = selectedFile.path
                    val toggleButton = findViewById<ToggleButton>(R.id.toggleButton)
                    toggleButton.isEnabled = true
                }
            }
        }

    private fun saveConfigAsync() {
        GlobalScope.launch {
            saveConfig()
        }
    }

    private fun saveConfig() {
        Paper.book().write("config", config)
    }

    private fun updateLocation() {
        getLocation()
    }

    private fun setLocation() {
        if (config.currentLocation == null) {
            return
        }
        val location1 = findViewById<TextView>(R.id.tv_coordinates1)
        location1.text = "Longitude: " + config.currentLocation!!.longitude.toString()
        val location2 = findViewById<TextView>(R.id.tv_coordinates2)
        location2.text = "Latitude: " + config.currentLocation!!.latitude.toString()
        mService?.location = config.currentLocation
    }

    override fun onDestroy() {
        super.onDestroy()
        job?.cancel()
        if (mIsBound == true) {
            unbindService()
            mIsBound = false
        }
    }

    /**
     * Interface for getting the instance of binder from our service class
     * So client can get instance of our service class and can directly communicate with it.
     */
    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, iBinder: IBinder) {
            // We've bound to MyService, cast the IBinder and get MyBinder instance
            val binder = iBinder as AudioService.MyBinder
            mService = binder.service
            mIsBound = true
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mIsBound = false
        }
    }

    /**
     * Used to bind to our service class
     */
    private fun bindService() {
        Intent(this, AudioService::class.java).also { intent ->
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
        }
    }

    /**
     * Used to unbind and stop our service class
     */
    private fun unbindService() {
        Intent(this, AudioService::class.java).also { intent ->
            unbindService(serviceConnection)
        }
    }

    fun getLocation() {
        when {
            ContextCompat.checkSelfPermission(
                applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED -> {
                // You can use the API that requires the permission.
                lm.requestLocationUpdates(provider!!, MIN_UPDATE_TIME, MIN_UPDATE_DISTANCE.toFloat(), this)
            }
            else -> {
                // You can directly ask for the permission.
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    1
                )
            }
        }
    }

    override fun onLocationChanged(newLoc: Location) {
        config.currentLocation = newLoc
        setLocation()
        GlobalScope.launch {
            saveConfig()
        }
        lm.removeUpdates(this)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                getLocation()
            }
        }
    }

    override fun onProviderDisabled(arg0: String) {}
    override fun onProviderEnabled(arg0: String) {}
    override fun onStatusChanged(arg0: String, arg1: Int, arg2: Bundle) {}

    companion object {
        // The minimum distance to change Updates in meters
        private const val MIN_UPDATE_DISTANCE: Long = 10

        // The minimum time between updates in milliseconds
        private const val MIN_UPDATE_TIME = (1000 * 60).toLong()
    }
}

class Config {
    var audioFile: String? = null
    var currentLocation: Location? = null
}

object Constants {
    const val NO_LOCATION = "Unknown coordinates"
    const val NO_AUDIO_FILE = "No file selected"
}