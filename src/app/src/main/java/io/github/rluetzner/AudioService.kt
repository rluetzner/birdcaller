package io.codeberg.rluetzner

import android.app.*
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Color
import android.location.Location
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.os.PowerManager
import androidx.annotation.RequiresApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.shredzone.commons.suncalc.SunTimes
import java.time.DayOfWeek
import java.time.LocalDateTime

class AudioService : Service() {
    private var wakeLock: PowerManager.WakeLock? = null

    // We'll bind the service to itself, so it stays running in the background.
    private var mService: AudioService? = null
    private var mIsBound: Boolean? = null

    //Instance of inner class created to provide access  to public methods in this class
    private val localBinder: IBinder = MyBinder()
    private var mMediaPlayer: MediaPlayer? = null
    private val CHANNEL_ID = "Bird Caller Service"
    var contentUri: Uri? = null
    private var job: Job? = null
    private var currentDayOfWeek: DayOfWeek? = null
    private var currentSuntime: SunTimes? = null
    var location: Location? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate() {
        super.onCreate()
        run()
        bindService()
        // we need this lock so our service gets not affected by Doze Mode
        wakeLock =
            (getSystemService(Context.POWER_SERVICE) as PowerManager).run {
                newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "BirdCallerService::lock").apply {
                    acquire()
                }
            }
        val notification = createNotification()
        startForeground(1, notification)
    }

    private fun createNotification(): Notification {
        val notificationChannelId = CHANNEL_ID

        // depending on the Android API that we're dealing with we will have
        // to use a specific method to create the notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager;
            val channel = NotificationChannel(
                notificationChannelId,
                "Bird Caller Service notifications channel",
                NotificationManager.IMPORTANCE_HIGH
            ).let {
                it.description = "Bird Caller Service channel"
                it.enableLights(true)
                it.lightColor = Color.RED
                it.enableVibration(true)
                it.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                it
            }
            notificationManager.createNotificationChannel(channel)
        }

        val pendingIntent: PendingIntent = Intent(this, MainActivity::class.java).let { notificationIntent ->
            PendingIntent.getActivity(this, 0, notificationIntent, 0)
        }

        val builder: Notification.Builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) Notification.Builder(
            this,
            notificationChannelId
        ) else Notification.Builder(this)

        return builder
            .setContentTitle("Bird Caller")
            .setContentText("Playing audio")
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setPriority(Notification.PRIORITY_HIGH) // for under android 26 compatibility
            .build()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun run() {
        job = GlobalScope.launch {
            while (true) {
                val date = LocalDateTime.now()
                if (currentDayOfWeek != date.dayOfWeek) {
                    currentDayOfWeek = date.dayOfWeek
                    val midnight = LocalDateTime.of(date.year, date.month, date.dayOfMonth, 0, 0)
                    do {
                        currentSuntime = getSuntime(midnight)
                    } while (currentSuntime == null)
                }

                if (currentSuntime!!.rise?.toLocalDateTime()!! <= date &&
                    currentSuntime!!.set?.toLocalDateTime()!! >= date) {
                    if (mMediaPlayer == null && contentUri != null) {
                        playContentUri(contentUri!!)
                    } else if (mMediaPlayer != null && contentUri == null) {
                        stopSound()
                    }
                } else if (mMediaPlayer != null) {
                    stopSound()
                }

                // Sleep for a second.
                delay(1000)
            }
        }
    }

    fun getSuntime(dateTime: LocalDateTime): SunTimes? {
        if (location == null) {
            return null
        }
        return SunTimes.compute()
            .on(dateTime)
            .at(location!!.latitude, location!!.longitude)
            .execute()
    }

    override fun onBind(intent: Intent): IBinder? {
        return localBinder
    }

    /**
     * This method is  Called when activity have disconnected from a particular interface published by the service.
     * Note: Default implementation of the  method just  return false  */
    override fun onUnbind(intent: Intent): Boolean {
        return super.onUnbind(intent)
    }

    /**
     * Called when an activity is connected to the service, after it had
     * previously been notified that all had disconnected in its
     * onUnbind method.  This will only be called by system if the implementation of onUnbind method was overridden to return true.
     */
    override fun onRebind(intent: Intent) {
        super.onRebind(intent)
    }

    /**
     * Called by the system to notify a Service that it is no longer     used and is being removed.  The
     * service should clean up any resources it holds (threads,       registered
     * receivers, etc) at this point.  Upon return, there will be no more calls
     * in to this Service object and it is effectively dead.
     */
    override fun onDestroy() {
        super.onDestroy()
        job?.cancel()
        if (mMediaPlayer != null) {
            mMediaPlayer!!.release()
            mMediaPlayer = null
        }
        try {
            wakeLock?.let {
                if (it.isHeld) {
                    it.release()
                }
            }
            stopForeground(true)
            stopSelf()
        } catch (e: Exception) {
        }
    }

    private fun playContentUri(uri: Uri) {
        if (mMediaPlayer != null) {
            stopSound()
        }

        mMediaPlayer = MediaPlayer().apply {
            setDataSource(application, uri)
            setAudioAttributes(
                AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()
            )
            prepare()
        }
        mMediaPlayer!!.isLooping = true
        mMediaPlayer!!.start()
    }

    private fun stopSound() {
        if (mMediaPlayer != null) {
            mMediaPlayer!!.stop()
            mMediaPlayer!!.release()
            mMediaPlayer = null
        }
        if (mIsBound == true) {
            unbindService()
        }
    }

    /**
     * Interface for getting the instance of binder from our service class
     * So client can get instance of our service class and can directly communicate with it.
     */
    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, iBinder: IBinder) {
            // We've bound to MyService, cast the IBinder and get MyBinder instance
            val binder = iBinder as AudioService.MyBinder
            mService = binder.service
            mIsBound = true
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mIsBound = false
        }
    }

    /**
     * Used to bind to our service class
     */
    private fun bindService() {
        Intent(this, AudioService::class.java).also { intent ->
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
        }
    }

    /**
     * Used to unbind and stop our service class
     */
    private fun unbindService() {
        Intent(this, AudioService::class.java).also { intent ->
            unbindService(serviceConnection)
        }
    }

    inner class MyBinder : Binder() {
        val service: AudioService
            get() =
                this@AudioService
    }
}