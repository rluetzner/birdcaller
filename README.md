![Bird Caller logo](images/logo.svg)

# Bird caller

This is a simple Android app that is meant to continously play bird sounds from sunup until sundown.

## Features

- Plays back an arbitrary audio file from sunrise to sunset.
- Location is determined via GPS.
- Sunrise and -set times are calculated offline.
- Audio runs on a foreground service, i.e. the app can be sent to the background.
- Partial Wake-Lock to prevent the service from ever stopping.

![Bird Caller UI](images/ui.png)

## Build from commandline

The following process expects you to have Android Studio installed.

```bash
# Run from the 'src' directory in the Git repository
./gradlew assemble
# The APKs can be found in the 'debug' and 'release' directories at app/build/outputs/apk.
```

## Logo

The logo is a combination of two public domain images from https://openclipart.org.

| Image | Author |
| ----- | ------ |
| black bird  | [DANI ELA](https://openclipart.org/artist/DANI%20ELA)     |
| Megaphone   | [Minduka](https://openclipart.org/artist/Minduka)         |
